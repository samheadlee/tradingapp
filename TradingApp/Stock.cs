﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace TradingApp
{
    class Stock
    {
        private static readonly HttpClient client = new HttpClient();

        public static string PersistStocks(string[] tickers)
        {
            string sql = "SELECT * FROM [TradingApp].[dbo].[AlphaVantageAPIRequests]";
            DataTable outputDt = Database.Select(sql);
            string[] outputArr = Database.DumpDataTable(outputDt).Split(",", StringSplitOptions.RemoveEmptyEntries);

            DateTime lastRequestTime = Convert.ToDateTime(outputArr[0]);
            int numberOfRequests = Convert.ToInt32(outputArr[1]);
            int allowedRequestsPer5Min = Convert.ToInt32(outputArr[2]);

            if (allowedRequestsPer5Min >= tickers.Length)
            {
                if (DateTime.Now.AddMinutes(-10) > lastRequestTime)
                {
                    for (int i = 0; i < tickers.Length; i++)
                    {
                        String x = Stock.PersistStock(tickers[i]).Result;
                    }

                    string updatelastRequestTimeSQL = "UPDATE [TradingApp].[dbo].[AlphaVantageAPIRequests] SET lastRequestTime = '" + DateTime.Now + "'";
                    Database.Insert(updatelastRequestTimeSQL);

                    string updateNumberOfRequestsSQL = "UPDATE [TradingApp].[dbo].[AlphaVantageAPIRequests] SET numberOfRequests = '" + tickers.Length + "'";
                    Database.Insert(updateNumberOfRequestsSQL);

                }
                else if ((allowedRequestsPer5Min - numberOfRequests) >= tickers.Length)
                {
                    for (int i = 0; i < tickers.Length; i++)
                    {
                        String x = Stock.PersistStock(tickers[i]).Result;
                    }

                    string updatelastRequestTimeSQL = "UPDATE [TradingApp].[dbo].[AlphaVantageAPIRequests] SET lastRequestTime = '" + DateTime.Now + "'";
                    Database.Insert(updatelastRequestTimeSQL);

                    string updateNumberOfRequestsSQL = "UPDATE [TradingApp].[dbo].[AlphaVantageAPIRequests] SET numberOfRequests = '" + (numberOfRequests + tickers.Length).ToString() + "'";
                    Database.Insert(updateNumberOfRequestsSQL);
                }
            } 

            return "true";
        }

        public async static Task<string> PersistStock(string ticker)
        {
            string responseString = $"{await GetStock(ticker)}";
            string[] lines = responseString.Split(new[] { Environment.NewLine },StringSplitOptions.RemoveEmptyEntries);

            foreach (String line in lines.Skip(1))
            { 
                String[] items = line.Split(new[] { "," }, StringSplitOptions.None);
                DateTime time = DateTime.Parse(items[0]);
                String open_price = items[1];
                String high_price = items[2];
                String low_price = items[3];
                String close_price = items[4];
                String volume = items[5];

                //lines below insert all stock data, except when a record with identical ticker and time already exists.
                String sql = "IF NOT EXISTS (SELECT * FROM [TradingApp].[dbo].[StockData] WHERE ticker = '" + ticker + "' AND time = '" + time + "') INSERT INTO [TradingApp].[dbo].[StockData] (ticker, time, open_price, high_price, low_price, close_price, volume) VALUES ('"
                    + ticker + "','" + time + "'," + open_price + "," + high_price + "," + low_price + "," + close_price + "," + volume + ")";

                Database.Insert(sql);
                
            }
            return "true";
        }



        public static async Task<string> GetStock(string ticker)
        {
            String url = "http://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=" + ticker + "&interval=5min&apikey=RMY5BEPXCVL6UHBR&datatype=csv";

            string ret = $"{await client.GetStringAsync(url)}";

            return ret;
        }
    }
}
