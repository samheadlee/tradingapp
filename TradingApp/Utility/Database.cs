﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace TradingApp
{
    class Database
    {
        private static string connectionString = "Server=localhost\\SQLEXPRESS;Database=TradingApp;User Id=sa;Password=tessie.55;";

        public static void Insert(String SQLQuery)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(SQLQuery, conn))
                    {
                        cmd.ExecuteNonQuery();
                        
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static DataTable Select(String SQLQuery)
        {
            DataTable output = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(SQLQuery, conn))
                    {
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            output.Load(dr);
                            return output;
                        }
                    }
                }
            }

            catch (SqlException)
            {
                return output;
            }
        }

        public static string DumpDataTable(DataTable table)
        {
            string data = string.Empty;
            StringBuilder sb = new StringBuilder();

            if (null != table && null != table.Rows)
            {
                foreach (DataRow dataRow in table.Rows)
                {
                    foreach (var item in dataRow.ItemArray)
                    {
                        sb.Append(item);
                        sb.Append(",");
                    }
                    sb.AppendLine();
                }

                data = sb.ToString();
            }
            return data;
        }
    }
}
