﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradingApp.Utility
{
    class Etc
    {
        public static void ClearDatabase()
        {
            string sql = "DELETE FROM [TradingApp].[dbo].[DailyReports];\n"
            + "DELETE FROM [TradingApp].[dbo].[StockData];\n"
            + "DELETE FROM [TradingApp].[dbo].[Strategies];\n"
            + "DELETE FROM [TradingApp].[dbo].[StrategiesEquityComposition];\n";
            Database.Insert(sql);
        }
    }
}
