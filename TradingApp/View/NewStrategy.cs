﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradingApp.View
{
    class NewStrategy
    {
        static string[] sectors = { "Consumer Staples", "Information Technology", "Health Care", "Industrials" };
        static string[] sectorETFTickers = { "VCR", "VGT", "VHT", "VIS" };

        public static bool View(String userID)
        {
            Console.WriteLine("\nEnter strategy name:");
            String strategy = Console.ReadLine();

            Console.WriteLine("\nEnter your initial investment ($):");
            int initialInvestment = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("\nRate your confidence in each sector from 1 to 5:");

            List<decimal> sectorConf = new List<decimal>();

            foreach (String sector in sectors)
            {
                Console.WriteLine(sector + ":");
                sectorConf.Add(Convert.ToDecimal(Console.ReadLine()));
            }

            Console.WriteLine("\nSubmit strategy? (y/n)");
            String response = Console.ReadLine();

            if (response == "y")
            {
                sectorConf.ForEach(Console.WriteLine);
                Strategies.CreateStrategy(userID, strategy, initialInvestment, sectorConf);
                Console.WriteLine("\nStrategy submitted.");
            }
            else
            {
                Console.WriteLine("\nStrategy was not submitted");
            }

            Console.WriteLine("\nCreate another strategy? (y/n)");
            String viewAnotherStrat = Console.ReadLine();
            bool createAnotherStrat = viewAnotherStrat == "y";
            return createAnotherStrat;
        }
    }
}
