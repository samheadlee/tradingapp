﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradingApp.View
{
    class CurrentStrategies
    {
        public static bool View(String userID)
        {
            Console.WriteLine("\nPlease enter strategy name listed below, or 'all' to view all");
            string[] strategies = Strategies.getStrategies(userID);

            foreach (string strat in strategies)
            {
                Console.WriteLine(strat);
            }

            String chosenStrategy = Console.ReadLine();

            if (chosenStrategy == "all")
            {
                foreach (string strategy in strategies)
                {
                    var dailyChange = Strategies.getDailyChange(userID, strategy);
                    Console.WriteLine(strategy + " had a change of $" + dailyChange + " today.");
                }

            }
            else
            {
                var dailyChange = Strategies.getDailyChange(userID, chosenStrategy);
                Console.WriteLine(chosenStrategy + " had a change of $" + dailyChange + " today.");
            }

            Console.WriteLine("\nView different strategy? (y/n)");
            bool viewAnotherStrat = (Console.ReadLine() == "y");

            return viewAnotherStrat;
        }

    }
}
