﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace TradingApp
{
    class Strategies
    {
        static string[] sectors = { "Consumer Staples", "Information Technology", "Health Care", "Industrials" };
        static string[] sectorETFTickers = { "VCR", "VGT", "VHT", "VIS" };

        public static void CreateStrategy(String userID, String strategy, int initialInvestment, List<decimal>sectorConf)
        {

            decimal[] sectorConfidence = sectorConf.ToArray();

            String confSQL = "INSERT INTO [TradingApp].[dbo].[Strategies] (UserID,Strategy,InitialInvestment,ConsumerStaplesConf,InformationTechnologyConf,HealthCareConf,IndustrialsConf) VALUES ('"
                        + userID + "','" + strategy + "'," + initialInvestment + "," + sectorConfidence[0] + "," + sectorConfidence[1] + "," + sectorConfidence[2] + "," + sectorConfidence[3] + ")";

            Database.Insert(confSQL);

            decimal totalConfidencePoints = sectorConf.Sum();
            decimal[] normalizedSectorConfidences = sectorConfidence.Select(r => r / totalConfidencePoints).ToArray();
            decimal[] maxSpendsPerSector = normalizedSectorConfidences.Select(r => r * initialInvestment).ToArray();
            List<decimal> sectorETFSharesList = new List<decimal>();
            List<decimal> uninvestedCapitalList = new List<decimal>();

            for (int i = 0; i < sectorConfidence.Length; i++)
            {
                string sql = "SELECT close_price FROM [TradingApp].[dbo].[StockData] WHERE time IN(SELECT max(time) FROM [TradingApp].[dbo].[StockData]) AND ticker ='" + sectorETFTickers[i] + "'";
                DataTable dt = Database.Select(sql);
                String data = Database.DumpDataTable(dt).Replace(",", "");
                decimal currentPrice = Convert.ToDecimal(data);

                int sectorETFShare = Convert.ToInt32(Math.Floor(maxSpendsPerSector[i] / currentPrice));
                decimal uninvestedCapitalAmt = maxSpendsPerSector[i] - (sectorETFShare * currentPrice);

                sectorETFSharesList.Add(sectorETFShare);
                uninvestedCapitalList.Add(uninvestedCapitalAmt);
            }

            decimal[] sectorETFShares = sectorETFSharesList.ToArray();
            decimal[] uninvestedCapital = uninvestedCapitalList.ToArray();

            decimal cashRemaining = uninvestedCapital.Sum();

            String sharesSQL = "INSERT INTO [TradingApp].[dbo].[StrategiesEquityComposition] (UserID,Strategy,SharesVCR,SharesVGT,SharesVHT,SharesVIS,CashRemaining) VALUES ('"
                        + userID + "','" + strategy + "'," + sectorETFShares[0] + "," + sectorETFShares[1] + "," + sectorETFShares[2] + "," + sectorETFShares[3] + "," + cashRemaining + ")";
            Database.Insert(sharesSQL);
        }

        public static decimal getDailyChange(String userID, String strategy)
        {
            String sql = "SELECT * FROM [TradingApp].[dbo].[StrategiesShareComposition] WHERE UserID = '" + userID + "' AND Strategy = '" + strategy + "'";
            DataTable res = Database.Select(sql);
            String data = Database.DumpDataTable(res);
            string[] sectorShares = data.Split(',', StringSplitOptions.RemoveEmptyEntries).ToArray();

            //Stock.PersistMultipleStocksData(sectorETFTickers);
            List<decimal> changes = new List<decimal>();

            for (int j = 0; j < sectorETFTickers.Length; j++)
            {
                DateTime start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 9, 30, 0);
                String startSQL = "SELECT * FROM [TradingApp].[dbo].[StockData] WHERE ticker = '" + sectorETFTickers[j] + "' AND time = '" + start + "'";
                DataTable startTable = Database.Select(startSQL);
                String startData = Database.DumpDataTable(startTable);
                decimal startPrice = Convert.ToDecimal(startData.Split(',')[2]);

                DateTime end = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0).AddMinutes(-10);
                String endSQL = "SELECT * FROM [TradingApp].[dbo].[StockData] WHERE ticker = '" + sectorETFTickers[j] + "' AND time = '" + end + "'";
                DataTable endTable = Database.Select(endSQL);
                String endData = Database.DumpDataTable(endTable);
                decimal endPrice = Convert.ToDecimal(endData.Split(',')[5]);

                decimal changePerShare = endPrice - startPrice; 
                decimal netChange = changePerShare * Convert.ToDecimal(sectorShares[(j+2)]);

                changes.Add(netChange);
            }

            decimal[] sectorConfidence = changes.ToArray();
            decimal dailyChange = changes.Sum();

            return dailyChange;
        }

        public static string[] getStrategies(string userID)
        {
            String sql = "SELECT DISTINCT Strategy FROM [TradingApp].[dbo].[StrategiesEquityComposition] WHERE UserID='" + userID + "'";

            DataTable strategiesTable = Database.Select(sql);
            String strategiesStr = Database.DumpDataTable(strategiesTable).Replace(System.Environment.NewLine, "");
            string[] strategies = strategiesStr.Split(",", StringSplitOptions.RemoveEmptyEntries);

            return strategies;
        }
    }
}
