﻿using System;
using System.Data.SqlClient;
using System.Text;
using TradingApp.Utility;
using TradingApp.View;

namespace TradingApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Etc.ClearDatabase();

            Console.WriteLine("\nWelcome To TradingApp!");
            Console.WriteLine("\nLOADING...\n");

            string[] sectorETFTickers = { "VCR", "VGT", "VHT", "VIS" };
            Stock.PersistStocks(sectorETFTickers);

            string[] benchmarkETFTickers = { "SPY" };
            Stock.PersistStocks(benchmarkETFTickers);

            string userID = Login.login();

            bool inApp = true;

            switch (inApp)
            {
                case true:
                    Console.WriteLine("Please select a task:");
                    Console.WriteLine("View current strategies (press v)");
                    Console.WriteLine("Create new strategy (press c)");
                    Console.WriteLine("Exit application (press x)");

                    var input = Console.ReadLine();

                    if (input == "v") { input = "ViewCurrentStrategies"; }
                    if (input == "c") { input = "CreateStrategy"; }
                    if (input == "x") { input = "Exit"; }

                    switch (input)
                    {
                        case "ViewCurrentStrategies":

                            bool viewAnotherStrat = CurrentStrategies.View(userID);
                            if (viewAnotherStrat) { goto case "ViewCurrentStrategies"; }
                            else break;

                        case "CreateStrategy":

                            bool createAnotherStrat = NewStrategy.View(userID);
                            if (createAnotherStrat) { goto case "CreateStrategy"; }
                            else break;

                        case "Exit":

                            inApp = false;
                            break;

                    }

                    if (inApp) { goto case true; }
                    else { break; }

                case false:
                    break;
            }

            //not sure how to incorporate switch user feature. prbly need to restructure
            //Console.WriteLine("\nSwitch user? (y/n)");
            //Console.ReadLine();

            Console.WriteLine("\nGoodbye");
            Console.ReadLine();
        }
    }
}
